#ifndef _TETRIS_H_
#define _TETRIS_H_

#include <curses.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

//サイズに関する変数
extern int LINE;    //画面に映るステージの高さ
extern int COL;     //画面に映るステージの幅
extern int L_COL;   //ステージの左壁の座標
extern int LOOPWAIT;//ループするときのカウント最大値
extern int HEIGHT;  //ステージ全体の高さ
extern int WIDTH;   //ステージ全体の幅
extern int R_COL;   //ステージの右壁の座標

#endif
