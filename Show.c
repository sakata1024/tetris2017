#include "Tetris.h"
#include "Game.h"

//ステージの枠を表示する関数
void Show_stageArea() {
  int i, j;

  //ステージの天井の作成
  mvaddch(0, L_COL, '*');
  for (j = 1; j <= WIDTH; j++)
    addch('=');
  addch('*');

  //ステージの壁の作成
  for (i = 1; i < LINES; i++) {
    mvaddch(i, L_COL, '|');
    mvaddch(i, R_COL, '|');
  }
}

//右側のステータスを表示する関数
void ShowStatus(int lapse, int score, int line, int level) {
  mvprintw(3, R_COL + 10, "Time  : %7d", lapse);
  mvprintw(4, R_COL + 10, "Score : %7d", score);
  mvprintw(5, R_COL + 10, "Line  : %7d", line);
  mvprintw(6, R_COL + 10, "Level : %7d", level);
}

//落下予定ブロックを表示する関数
void Show_nextBlock(nextBlock *block) {
  int i, j, cnt = 0;
  nextBlock *tmp;

  //blockが直接参照していないブロックを表示する
  tmp = block->next;

  attron(A_BOLD); //太字設定

  //落下予定ブロックの表示
  while (block != tmp) {
    for (i = 0; i < 3; i++) {
      for (j = 0; j < 3; j++) {
	if (tmp->blockdata.block[i][j] == 1) {
	  attron(COLOR_PAIR(tmp->patternNum + 1));
	  mvaddch(2 + 4 * cnt + i, R_COL + 2 + j, '#');
	}
	else {
	  attron(COLOR_PAIR(8));
	  mvaddch(2 + 4 * cnt + i, R_COL + 2 + j, ' ');
	}
      }
    }
    cnt++;
    tmp = tmp->next;
  }
  
  attroff(A_BOLD); //太字解除
  attron(COLOR_PAIR(8));
}

//ブロックを表示する関数
void Show_Block(int X, int Y, nextBlock *block, char ch) {
  int i, j;
  
  attron(A_BOLD); //太字設定
  
  for (i = 0; i < 3; i++) {
    for (j = 0; j < 3; j++) {
      if (block->blockdata.block[i][j] == 1 && Y + i >= 2) {
	attron(COLOR_PAIR(block->patternNum + 1));
	mvaddch(Y - 1 + i, X + L_COL + 1 + j, ch);
      }
    }
  }
  
  attroff(A_BOLD); //太字解除
  attron(COLOR_PAIR(8));
}

//ホールドしたブロックを表示する関数
void Show_Hold(nextBlock hold) {
  int i, j;
  
  //ホールドにブロックがあれば表示する
  if (hold.patternNum != -1) {
    attron(A_BOLD);
    
    for (i = 0; i < 3; i++) {
      for (j = 0; j < 3; j++) {
	if (hold.blockdata.block[i][j] == 1) {
	  attron(COLOR_PAIR(hold.patternNum + 1));
	  mvaddch(2 + i, L_COL - 4 + j, '#');
	  
	}
	else {
	  attron(COLOR_PAIR(8));
	  mvaddch(2 + i, L_COL - 4 + j, ' ');
	}
      }
    }
    
    attroff(A_BOLD);
    attron(COLOR_PAIR(8));
  }
  
}


//ステージのマスを表示する関数
void Show_stage(block **stageData) {
  int i, j;

  attron(A_BOLD);
  
  for (i = 2; i < HEIGHT; i++) {
    move(i - 1, L_COL + 1); //カーソル移動
    
    for (j = 0; j < WIDTH; j++) {
      attron(COLOR_PAIR(stageData[i][j].color + 1));
      
      if (stageData[i][j].blc == 0)
	addch(' ');
      else
	addch('#');
    }
  }
  
  attroff(A_BOLD);
  attron(COLOR_PAIR(8));
}
