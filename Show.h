#ifndef _SHOW_H_
#define _SHOW_H_

#include "Tetris.h"
#include "Game.h"

void Show_stageArea();
void ShowStatus(int lapse, int score, int line, int level);
void Show_nextBlock(nextBlock *block);
void Show_Block(int X, int Y, nextBlock *block, char ch);
void Show_stage(block **stageData);
void Show_Hold(nextBlock hold);

#endif

