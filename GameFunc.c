#include "Tetris.h"
#include "Game.h"

//ブロックの座標の調整
void Adjust_XY(int *blocX, int *blocY, block **stageData, nextBlock nextblock, int ch) {
  int i, j;

  switch (ch) {
  case KEY_LEFT:
    for (i = 0; i < 3; i++) {
      for (j = 0; j < 3; j++) {
	//縦にはみ出す場所は考えない
	if (*blocY + i >= 0 && *blocY + i < HEIGHT) {
	  //左に移動してブロックがあったら戻す
	  if (stageData[*blocY + i][*blocX + j].blc == 1 && nextblock.blockdata.block[i][j] == 1) {
	    *blocX += 1;
	    return;
	  }
	}
	//左壁にはみ出したら戻す
	if (*blocX + j < 0 && nextblock.blockdata.block[i][j] == 1) {
	  *blocX = -j;
	  return;
	}
      }
    }
    break;
    
  case KEY_RIGHT:
    for (i = 2; i >= 0; i--) {
      for (j = 2; j >= 0; j--) {
	//縦にはみ出す場所は考えない
	if (*blocY + i >= 0 && *blocY + i < HEIGHT) {
	  //右に移動してブロックがあったら戻す
	  if (stageData[*blocY + i][*blocX + j].blc == 1 && nextblock.blockdata.block[i][j] == 1) {
	    *blocX -= 1;
	    return;
	  }
	}
	//右壁にはみ出したら戻す
	if (*blocX + j >= WIDTH && nextblock.blockdata.block[i][j] == 1) {
	  *blocX = WIDTH - j - 1;
	  return;
	}
      }
    }
    break;
    
  default:
    break;
  }
  
}

//ステージ情報を記録する関数
void Record_stage(int blocX, int blocY, nextBlock next, block **stageData) {
  int i, j;
  
  for (i = 0; i < 3; i++) {
    for (j = 0; j < 3; j++) {
      //ブロックがあればstageDataに書き込む
      if (next.blockdata.block[i][j] == 1 && blocY + i > 0) {
	stageData[blocY + i][blocX + j].blc = 1;
	stageData[blocY + i][blocX + j].color = next.patternNum;
      }
    }
  }
}

//下にある壁との衝突判定をする関数
int CollisionBottomWall(int blocX, int blocY, int bottom, nextBlock nextblock)
{
  int i, j;

  for (i = 2; i >= 0; i--) {
    for (j = 2; j >= 0; j--) {
      //ブロックの下に壁がある場合1を返す
      if (nextblock.blockdata.block[i][j] == 1 && blocY + i + 1 >= HEIGHT)
	return 1;
    }
  }
  return 0;
}

//既にあるブロックとの衝突判定
int CollisionBlock(int blocX, int blocY, block **stageData, nextBlock nextblock) {
  int i, j;
  
  for (i = 2; i >= 0; i--) {
    //縦にはみ出すものは考えない
    if (blocY + i < 0 || blocY + i + 1 >= HEIGHT)
      continue;
    
    for (j = 2; j >= 0; j--) {
      //落下ブロックの下にブロックがあった場合1を返す
      if (nextblock.blockdata.block[i][j] == 1 &&
	  stageData[blocY + i + 1][blocX + j].blc == 1) {
	return 1;
      }
    }
  }
  
  return 0;
}

//ブロックの回転をする関数
void RotationBlock(int *blocX, int *blocY, nextBlock *nextblock, int direction, block **stageData) {
  blockData blockdata;
  int i, j;
  int oldX = *blocX, oldY = *blocY;
  int upflag = 0;

  //四角型なら回転しない
  if (nextblock->patternNum == 1)
    return;
  
  //回転をする
  for (i = -1; i <= 1; i++) {
    for (j = -1; j <= 1; j++) {
      blockdata.block[j*direction + 1][-i*direction + 1] =
	nextblock->blockdata.block[i + 1][j + 1];
    }
  }
  
  //回転判定
  for (i = 0; i < 3; i++) {
    for (j = 0; j < 3; j++) {
      if (blockdata.block[i][j] == 1) {
	//天井を突き抜ける場合は考えない
	if (*blocY + i < 0) {
	  return;
	}
	//床に突き抜ければその分戻す
	if (*blocY + i >= HEIGHT) {
	  *blocY = HEIGHT - i - 1;
	  i = -1;
	  break;
	}
	//左の壁に突き抜ければその分戻す
	if (*blocX + j < 0) {
	  *blocX = -j;
	  i = -1;
	  break;
	}
	//右の壁に突き抜ければその分戻す
	if (*blocX + j >= WIDTH) {
	  *blocX = WIDTH - j - 1;
	  i = -1;
	  break;
	}
	//ブロックとの判定をする
	if (stageData[*blocY + i][*blocX + j].blc == 1) {
	  //修正をしてもブロックと重なるなら座標を戻し回転をしない
	  if (upflag == 2) {
	    *blocY = oldY;
	    *blocX = oldX;
	    return;
	  }
	  else { //ぶつかるならひとつ上のマスに移動する
	    upflag++;
	    *blocY -= 1;
	    i = -1;
	    break;
	  }
	}
      }
    }
  }
  
  //回転後のブロックを保存する
  nextblock->blockdata = blockdata;
}

//一列そろったら消す関数
int DeleteLine(block **stageData, int *score) {
  int i, j, ii, jj;
  int downline = 0;
  int breakflag = 0;
  static int magicflag = 0;

  for (i = HEIGHT - 1; i >= 0; i--) {
    for (j = 0; j < WIDTH; j++) {
      //一列のうちブロックがないマスがあれば次の列を探索する
      if (stageData[i][j].blc == 0) {
	breakflag = 1;
	break;
      }
    }
    
    //その行が削除対象なら消す
    if (!breakflag) {
      //消すべき行ならその行を消すように一列ずつずらす
      for (ii = i; ii - 1 >= 0; ii--) {
	for (jj = 0; jj < WIDTH; jj++) {
	  stageData[ii][jj].blc = stageData[ii - 1][jj].blc;
	  stageData[ii][jj].color = stageData[ii - 1][jj].color;
	}
      }
      downline++; //消した行を1増やす
      i++; //削除され上書きされた行を判定するためiを1増やす
    }
    breakflag = 0;
  }
  
  //ずらした後上書きされていない上部の行を初期化する
  for (; i >= 0; i--) {
    for (ii = 0; ii < WIDTH; ii++) {
      stageData[i][ii].blc = 0;
      stageData[i][ii].color = 7;
    }
  }
  
  //連続で3列消しをしたならスコアを増やす
  if (downline == 3)
    magicflag = 1;
  else if(downline != 0)
    magicflag = 0;
  
  //1行でも消しているならスコアを増やす
  if (downline)
    *score += 100 * downline * downline * (magicflag + 1);
  
  return downline;
}

//レベルアップを判定する関数
int LevelUpCount(int *level, int score) {
  //levelがスコアから出される値と違えばlevelを変更する
  if (*level != (int)(score / 5000) + 1) {
    *level = (int)(score / 5000) + 1;
    return 1;
  }
  return 0;
}

//waitCountを変更する関数
void ChangeWaitCount(int *waitCount, int level) {
  
  //waitCount9000以下なら2/3倍する
  if (*waitCount <= 9000)
    *waitCount = *waitCount * 2 / 3;
  //waitCountが9000以上なら4000を引く
  else
    *waitCount = LOOPWAIT - (level - 1) * 4000;
  
  //waitCountが万が一0以下になったら1にする
  if (*waitCount <= 0)
    *waitCount = 1;
  
}

//ホールドする関数
void Hold_Block(nextBlock **next, nextBlock *hold) {
  nextBlock tmp;
  
  //まだホールドされてなければホールドに格納し新たなブロックを出す
  if (hold->patternNum == -1) {
    hold->patternNum = (*next)->patternNum;
    hold->blockdata = blockPattern[hold->patternNum];
    Set_nextBlock(*next);
    *next = (*next)->next;
    
  }
  else { //ホールドと現在落下しているブロックを交換する
    tmp.patternNum = (*next)->patternNum;
    tmp.blockdata = blockPattern[tmp.patternNum];
    (*next)->patternNum = hold->patternNum;
    (*next)->blockdata = hold->blockdata;
    *hold = tmp;
  }
}
