#include "Tetris.h"
#include "Ranking.h"

//ランキング画面の関数
void Ranking() {
  FILE *fp;

  erase(); //画面初期化
  
  //ファイルが存在しなければ初期化する
  if ((fp = fopen("highscore.txt", "r")) == NULL) {
    
    InitRanking();
    
    fp = fopen("highscore.txt", "r");
  }
  
  ShowRanking(fp); //ランキングの表示
  
  fclose(fp);
  
  while (getch() != '\n'); //改行が押されれば戻る
}

//ランキングを表示する関数
void ShowRanking(FILE *fp) {
  char buf[256], name[256];
  int i = 5, score;
  
  mvprintw(LINES * 2 / 5 - 2, COLS / 2 - 11, "Ranking(best 5)");
  mvprintw(LINES * 2 / 5, COLS / 2 - 11, "No\tscore\tname");
  
  //読み込みが終わるまで出力する
  while (fgets(buf, 256, fp) != NULL) {
    sscanf(buf, "%d %[^\n]", &score, name);
    mvprintw(LINES * 2 / 5 + 1 + i, COLS / 2 - 11, "%d.\t%5d\t%s", i, score, name);
    i--;
  }
}

//ランキングのテキストを初期化する関数
void InitRanking() {
  int i;
  FILE *fp;

  fp = fopen("highscore.txt", "w");
  
  for (i = 0; i < 5; i++)
    fprintf(fp, "0 -----\n");

  fclose(fp);
}

//ランキングをテキストから読み込む関数
void LoadRanking(scoreData *scoreList) {
  FILE *fp;
  char buf[256];
  int i = 0;
  
  //ファイルがなければ初期化する
  if ((fp = fopen("highscore.txt", "r")) == NULL) {
    InitRanking();
    fp = fopen("highscore.txt", "r");
  }
  
  //ファイルから読み込み
  while (fgets(buf, 256, fp) != NULL) {
    sscanf(buf, "%d %[^\n]", &(scoreList[i].score), scoreList[i].name);
    i++;
  }
  
  fclose(fp);
}

//ランキングをファイルに出力する関数
void WriteRanking(scoreData *scoreList) {
  FILE *fp;
  int i;
  
  fp = fopen("highscore.txt", "w");
  
  for (i = 0; i < 5; i++) {
    fprintf(fp, "%d %s\n", scoreList[i].score, scoreList[i].name);
  }
  
  fclose(fp);
}
