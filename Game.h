#ifndef _GAME_H_
#define _GAME_H_

//状態の初期化
#define GAME_INIT(X, Y) X=WIDTH / 2 - 1; Y = 0

//ブロックの構造体
typedef struct {
  char block[3][3];
} blockData;

//ブロックパターン
extern const blockData blockPattern[7];

//ステージ1マスの構造体
typedef struct {
  unsigned blc : 1;
  unsigned color : 3;
} block;

//落下するブロックの構造体
struct next {
  blockData blockdata;
  int patternNum;
  struct next *next;
};
typedef struct next nextBlock;

//スコアの構造体
typedef struct {
  char name[256];
  int score;
}scoreData;

/* Game.c */
void MainLoop();
void GameOver(scoreData *scoreList, int score);

/* GameSub.c */
void Init_stageData(block ***stageData);
void Free_stageData(block ***stageData);
void Init_nextBlock(nextBlock **nextblock, int num);
void Free_nextBlock(nextBlock **nextBlock);
void Set_nextBlock(nextBlock *next);
int next_rand();

/* GameFunc.c */
int CollisionBottomWall(int blocX, int blocY, int bottom, nextBlock nextblock);
int CollisionBlock(int blocX, int blocY, block **stageData, nextBlock nextblock);
void RotationBlock(int *blocX, int *blocY, nextBlock *nextBlock, int direction, block **stageData);
int DeleteLine(block **stageData, int *score);
int LevelUpCount(int *level, int score);
void ChangeWaitCount(int *waitCount, int level);
void Adjust_XY(int *blocX, int *blocY, block **stageData, nextBlock nextblock, int ch);
void Record_stage(int blocX, int blocY, nextBlock next, block **stageData);
void Hold_Block(nextBlock **next, nextBlock *hold);

/* Show.c */
void Show_stageArea();
void ShowStatus(int lapse, int score, int line, int level);
void Show_nextBlock(nextBlock *block);
void Show_Block(int X, int Y, nextBlock *block, char ch);
void Show_stage(block **stageData);
void Show_Hold(nextBlock hold);

#endif
