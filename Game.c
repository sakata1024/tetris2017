#include <string.h>
#include "Tetris.h"
#include "Game.h"
#include "Show.h"
#include "Ranking.h"

const blockData blockPattern[7] = {
  {
    {/* 1:長い棒 */
      { 0,0,0 },
      { 1,1,1 },
      { 0,0,0 }
    }
  },
  {
    {/* 2:四角 */
      { 1,1,0 },
      { 1,1,0 },
      { 0,0,0 }
    }
  },
  {
    {/* 3:T字型 */
      { 0,1,0 },
      { 1,1,1 },
      { 0,0,0 }
	}
  },
  {
    {/* 4:J字型 */
      { 1,0,0 },
      { 1,1,1 },
      { 0,0,0 }
    }
  },
  {
    {/* 5:L字型 */
      { 0,0,1 },
      { 1,1,1 },
      { 0,0,0 }
    }
  },
  {
    {/* 6:S字型 */
      { 0,1,1 },
      { 1,1,0 },
      { 0,0,0 }
    }
  },
  {
    {/* 7:Z字型 */
      { 1,1,0 },
      { 0,1,1 },
      { 0,0,0 }
    }
  }
  
};

//ゲームメインループの関数
void MainLoop() {
  int blocX, blocY; //3×3ブロックの左上のx座標y座標
  int oldX; //前フレームでのx座標の位置(落下確定待ちで使う)
  int delay = 1; //遅延のためのカウント変数
  int waitCount = LOOPWAIT; //1マス落下するまでのカウント数
  int deleteLine = 0; //削除したラインの総数
  int level = 1; //レベル
  int score = 0; //スコア
  int lapse = 0; //経過時間
  int movedelay = 1; //落下確定のためのカウント変数
  int movewaitflag = 0; //落下判定してるかフラグ
  int dropflag = 0; //ハードドロップしたかフラグ
  int moveflag = 0; //落下判定した後に動いているかフラグ
  int holdflag = 0; //ホールドしてるかフラグ
  int ch, i, ii;
  block **stageData = NULL; //ステージのマスの情報をもつポインタ
  nextBlock *nextblock = NULL; //落下ブロックのリストを持つポインタ
  nextBlock holdblock; //ホールドしているブロックの情報をもつ変数
  time_t game_time; //ゲーム開始した時点での時刻
  scoreData scoreList[5]; //ハイスコアを持つ配列

  erase(); //画面初期化

  //stageDataの動的メモリ確保
  Init_stageData(&stageData);

  //盤面の初期化
  Show_stageArea();

  //落下ブロックのリストを作成
  Init_nextBlock(&nextblock, 4);

  //ホールドブロックの初期化
  holdblock.patternNum = -1;

  //落下初期値の設定
  GAME_INIT(blocX, blocY);

  //ランキング読み込み
  LoadRanking(scoreList);

  //ゲーム開始
  mvprintw(LINES / 2, R_COL + 10, "Press Enter");
  while ((getch() != '\n')); //Enterでスタート

  mvprintw(LINES / 2, R_COL + 10, "           ");

  //ステータス等表示
  mvprintw(0, L_COL - 5, "HOLD");
  mvprintw(0, R_COL + 2, "NEXT");
  ShowStatus(lapse, score, deleteLine, level);

  //ランキング表示
  mvprintw(8, R_COL + 10, "    Ranking    ");
  for (i = 0; i < 5; i++)
    mvprintw(9 + i, R_COL + 8, "%d.\t%5d\t%s", i + 1, scoreList[4 - i].score, scoreList[4 - i].name);
  
  //ゲーム開始時間
  game_time = time(NULL);

  //メインループ
  while ((ch = getch()) != 'Q') {
    
    //落下予定ブロックの表示
    Show_nextBlock(nextblock);

    oldX = blocX;
    
    //前回ループのブロック位置の初期化
    Show_Block(blocX, blocY, nextblock, ' ');
    
	//キー操作
    switch (ch) {
    case KEY_LEFT: //左移動
      blocX -= 1;
      break;

    case KEY_RIGHT: //右移動
      blocX += 1;
      break;
      
    case KEY_UP: //ハードドロップ(一気に落下)
      dropflag = 1;
      break;
      
    case KEY_DOWN: //強制落下(落下スピード増加)
      delay = waitCount;
      score += 1;
      break;
      
    case ' ': //回転
      RotationBlock(&blocX, &blocY, nextblock, 1, stageData);
      moveflag = 1;
      break;
      
    case 'v': //逆回転
      RotationBlock(&blocX, &blocY, nextblock, -1, stageData);
      moveflag = 1;
      break;
      
    case 'z': //ホールド
      if (!holdflag) { //落ちるまで1回しかできない
	holdflag = 1;
	Hold_Block(&nextblock, &holdblock);
	Show_Hold(holdblock);
	GAME_INIT(blocX, blocY);
	movewaitflag = 0;
      }
      break;

    default:
      break;
    }
    
    //座標の調整
    Adjust_XY(&blocX, &blocY, stageData, *nextblock, ch);
    
    //ステージの表示
    Show_stage(stageData);
    
    //落下予想位置の判定
    for (ii = blocY; ii < HEIGHT; ii++) {
      if (CollisionBottomWall(blocX, ii, LINE, *nextblock) ||
	  CollisionBlock(blocX, ii, stageData, *nextblock)) {
	break;
      }
    }
    
    //落下予想位置の表示
    Show_Block(blocX, ii, nextblock, 'X');
    
    //落下ブロック表示
    Show_Block(blocX, blocY, nextblock, '#');
    
    /* 1マス分落下時の処理 */
    if (delay % waitCount == 0 || dropflag || movewaitflag) {
      delay = 0;
      
      //地面・ブロック衝突判定
      if ((CollisionBottomWall(blocX, blocY, LINE, *nextblock) ||
	   CollisionBlock(blocX, blocY, stageData, *nextblock))) {
	
	//移動していたらまだ動けるようにする
	if (oldX != blocX || moveflag == 1) {
	  movewaitflag = 1;
	  movedelay = 1;
	  moveflag = 0;
	}
	
	//状態確定処理
	if (movedelay / (LOOPWAIT - 5000) >= 1 || dropflag) {
	  movedelay = 0;
	  
	  //ラインを超えたらゲームオーバー
	  if (blocY <= 0) {
	    break;
	  }
	  
	  //ステージ情報の記録
	  Record_stage(blocX, blocY, *nextblock, stageData);
	  
	  //削除ライン判定
	  deleteLine += DeleteLine(stageData, &score);
	  
	  //レベルアップ判定
	  if (LevelUpCount(&level, score))
	    ChangeWaitCount(&waitCount, level);
	  
	  //フラグ等初期化
	  GAME_INIT(blocX, blocY);
	  dropflag = 0;
	  movewaitflag = 0;
	  holdflag = 0;
	  
	  //次のブロックに変更
	  Set_nextBlock(nextblock);
	  nextblock = nextblock->next;
	}
      }
      else { //地面と衝突していなければ1マス落ちる
	movedelay = 1;
	blocY += 1;
	if (dropflag)
	  score += 2;
      }
    }
    //毎フレーム処理
    delay++;
    movedelay++;
    lapse = (int)(time(NULL) - game_time);
    
    ShowStatus(lapse, score, deleteLine, level);
  }
  
  //ゲームオーバー処理
  GameOver(scoreList, score);

  //メモリ解放
  Free_stageData(&stageData);
  Free_nextBlock(&nextblock);

  //改行が押されればゲーム終了
  mvprintw(21, R_COL + 12, "Press Enter");
  while ((getch() != '\n'));
}

//ゲームオーバーに関する処理をする関数
void GameOver(scoreData *scoreList, int score) {
  int i;
  scoreData tmpScore;
  char buf[256];

  //ゲームオーバー表示
  attron(COLOR_PAIR(7));
  mvprintw(15, R_COL + 13, "Game Over");
  attron(COLOR_PAIR(8));

  //ランキングの最小値よりも大きい場合ランクインする
  if (scoreList[0].score < score) {
    mvprintw(16, R_COL + 13, "Rank In!!");
    mvprintw(17, R_COL + 2, "Please Enter Your Name(10 letters)");
    
    echo();
    curs_set(1);
    timeout(-1);
    
    //名前の入力
    mvscanw(18, R_COL + 8, "%10[^\n]", buf);
    
    noecho();
    curs_set(0);
    timeout(0);
    
    scoreList[0].score = score;
    strcpy(scoreList[0].name, buf);
    
    //スコアのソート
    for (i = 0; i < 4; i++) {
      if (scoreList[i].score > scoreList[i + 1].score) {
	tmpScore = scoreList[i];
	scoreList[i] = scoreList[i + 1];
	scoreList[i + 1] = tmpScore;
      }
    }
    
    //ハイスコアをテキストに書き込み
    WriteRanking(scoreList);

    mvprintw(20, R_COL + 10, "Save a Record!!");
  }
  
}
