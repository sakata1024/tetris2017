#include "Tetris.h"
#include "Game.h"

//stageDataのメモリ確保をする関数
void Init_stageData(block ***stageData) {
  int i, j;
  
  /* HEIGHT * WIDTHの2次元配列を動的確保 & 初期化 */
  *stageData = malloc(sizeof(block *)*HEIGHT);
  for (i = 0; i < HEIGHT; i++) {
    (*stageData)[i] = (block *)malloc(sizeof(block) * WIDTH);
    for (j = 0; j < WIDTH; j++) {
      (*stageData)[i][j].blc = 0;
      (*stageData)[i][j].color = 7;
    }
  }
}

//stageDataのメモリを解放する関数
void Free_stageData(block ***stageData) {
  int i;
  
  for (i = 0; i < HEIGHT; i++)
    free((*stageData)[i]);
  free(*stageData);
}

//落下予定ブロックの環状リストを生成する関数
void Init_nextBlock(nextBlock **nextblock, int num) {
  nextBlock *tmp;
  int i;

  //1個目のnextBlockを作成
  *nextblock = (nextBlock *)malloc(sizeof(nextBlock));
  Set_nextBlock(*nextblock);
  
  (*nextblock)->next = *nextblock;
  
  //加えて合計num個になるよう作成
  for (i = 1; i < num; i++) {
    tmp = (nextBlock *)malloc(sizeof(nextBlock));
    Set_nextBlock(tmp);
    
    //環状になるようにつなげる
    tmp->next = (*nextblock)->next;
    (*nextblock)->next = tmp;
    *nextblock = tmp;
  }
}

//環状リストのメモリ解放をする関数
void Free_nextBlock(nextBlock **nextblock) {
  nextBlock *tmp = (*nextblock)->next;
  nextBlock *ttmp;
  while (*nextblock != tmp) {
    ttmp = tmp;
    tmp = tmp->next;
    free(ttmp);
  }
  free(*nextblock);
}

//nextBlockの中身を作る関数
void Set_nextBlock(nextBlock *next) {
  int pattern = next_rand(); //落下ブロック用の乱数を生成
  
  //値の格納
  next->blockdata = blockPattern[pattern];
  next->patternNum = pattern;
}

//ブロック出現乱数を生成する関数
int next_rand() {
  static int choice = 7; //読み込みが何回目かを記憶する変数
  int i, r, tmp;
  static int a[7] = { 0,1,2,3,4,5,6 }; //ブロック番号のシャッフルを記憶する変数
  
  choice++;
  
  //配列を読み終わったらシャッフルする
  if (choice >= 7) {
    choice = 0;
    
    /* 番号のシャッフル */
    for (i = 0; i < 7; i++) {
      r = rand() % 7;
      tmp = a[r];
      a[r] = a[i];
      a[i] = tmp;
    }
  }
  
  return a[choice];
}
