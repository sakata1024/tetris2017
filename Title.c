#include "Tetris.h"
#include "Title.h"

//タイトル画面の関数
int Title() {
  int ch = 0;
  int cursor;
  
  //タイトル落下アニメーション
  ShowTitle();
  
  //セレクトメニュー表示
  mvprintw(LINES * 3 / 5, COLS / 2 - 7, "  1P Mode");
  mvprintw(LINES * 3 / 5 + 2, COLS / 2 - 7, "  Ranking");
  mvprintw(LINES * 3 / 5 + 4, COLS / 2 - 7, "  Exit");
  
  mvaddch(LINES * 3 / 5, COLS / 2 - 7, '>');
  cursor = 0;
  
  //カーソル移動
  while ((ch = getch()) != '\n') {
    switch (ch) {
    case KEY_UP:
      mvaddch(LINES * 3 / 5 + cursor * 2, COLS / 2 - 7, ' ');
      cursor--;
      if (cursor < 0)
	cursor = 2;
      mvaddch(LINES * 3 / 5 + cursor * 2, COLS / 2 - 7, '>');
      break;
      
    case KEY_DOWN:
      mvaddch(LINES * 3 / 5 + cursor * 2, COLS / 2 - 7, ' ');
      cursor++;
      if (cursor > 2)
	cursor = 0;
      mvaddch(LINES * 3 / 5 + cursor * 2, COLS / 2 - 7, '>');
      break;
      
    default:
      break;
    }
  }
  
  return cursor;
}

//タイトルのロゴを表示する関数
void ShowTitle() {
  int delay = 1;
  int i = 0, X, Y;
  
  while (i <= 9) {
    
    //delayがカウントに達するまでインクリメント
    if (delay % 15000000 != 0) {
      delay++;
      continue;
    }
    
    delay = 1;
    if (getch() == '\n')
      i = 9;
    
    for (X = 0; X < COLS; X++)
      for (Y = 0; Y < 10; Y++)
	mvaddch(Y, X, ' ');
    
    attron(A_BOLD);
    attron(COLOR_PAIR(1));
    mvprintw(i - 7, COLS / 2 - 36, "#############");
    mvprintw(i - 6, COLS / 2 - 36, "     ###     ");
    mvprintw(i - 5, COLS / 2 - 36, "     ###     ");
    mvprintw(i - 4, COLS / 2 - 36, "     ###     ");
    mvprintw(i - 3, COLS / 2 - 36, "     ###     ");
    mvprintw(i - 2, COLS / 2 - 36, "     ###     ");
    mvprintw(i - 1, COLS / 2 - 36, "     ###     ");
    mvprintw(i, COLS / 2 - 36,     "     ###     ");
    
    attron(COLOR_PAIR(7));
    mvprintw(i - 7, COLS / 2 - 22, "############");
    mvprintw(i - 6, COLS / 2 - 22, "##          ");
    mvprintw(i - 5, COLS / 2 - 22, "##          ");
    mvprintw(i - 4, COLS / 2 - 22, "############");
    mvprintw(i - 3, COLS / 2 - 22, "##          ");
    mvprintw(i - 2, COLS / 2 - 22, "##          ");
    mvprintw(i - 1, COLS / 2 - 22, "##          ");
    mvprintw(i, COLS / 2 - 22,     "############");
    
    attron(COLOR_PAIR(2));
    mvprintw(i - 7, COLS / 2 - 9, "#############");
    mvprintw(i - 6, COLS / 2 - 9, "     ###     ");
    mvprintw(i - 5, COLS / 2 - 9, "     ###     ");
    mvprintw(i - 4, COLS / 2 - 9, "     ###     ");
    mvprintw(i - 3, COLS / 2 - 9, "     ###     ");
    mvprintw(i - 2, COLS / 2 - 9, "     ###     ");
    mvprintw(i - 1, COLS / 2 - 9, "     ###     ");
    mvprintw(i, COLS / 2 - 9,     "     ###     ");
    
    attron(COLOR_PAIR(6));
    mvprintw(i - 7, COLS / 2 + 5, "########### ");
    mvprintw(i - 6, COLS / 2 + 5, "##        ##");
    mvprintw(i - 5, COLS / 2 + 5, "##        ##");
    mvprintw(i - 4, COLS / 2 + 5, "########### ");
    mvprintw(i - 3, COLS / 2 + 5, "####        ");
    mvprintw(i - 2, COLS / 2 + 5, "## ###      ");
    mvprintw(i - 1, COLS / 2 + 5, "##   ####   ");
    mvprintw(i, COLS / 2 + 5,     "##      ####");
    
    attron(COLOR_PAIR(5));
    mvprintw(i - 7, COLS / 2 + 19, "##");
    mvprintw(i - 6, COLS / 2 + 19, "##");
    mvprintw(i - 5, COLS / 2 + 19, "  ");
    mvprintw(i - 4, COLS / 2 + 19, "##");
    mvprintw(i - 3, COLS / 2 + 19, "##");
    mvprintw(i - 2, COLS / 2 + 19, "##");
    mvprintw(i - 1, COLS / 2 + 19, "##");
    mvprintw(i, COLS / 2 + 19,     "##");
    
    attron(COLOR_PAIR(3));
    mvprintw(i - 7, COLS / 2 + 23, "  ########## ");
    mvprintw(i - 6, COLS / 2 + 23, "###      ####");
    mvprintw(i - 5, COLS / 2 + 23, "##           ");
    mvprintw(i - 4, COLS / 2 + 23, " ######      ");
    mvprintw(i - 3, COLS / 2 + 23, "      ###### ");
    mvprintw(i - 2, COLS / 2 + 23, "           ##");
    mvprintw(i - 1, COLS / 2 + 23, "####      ###");
    mvprintw(i, COLS / 2 + 23,     " ##########  ");
    
    i++;
  }
  
  attroff(A_BOLD);
  attron(COLOR_PAIR(8));
}
