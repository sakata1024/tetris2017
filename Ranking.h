#ifndef _RANKING_H_
#define _RANKING_H_

#include "Game.h"
#include <stdio.h>

void Ranking();
void ShowRanking(FILE *fp);
void InitRanking();
void LoadRanking(scoreData *scoreList);
void WriteRanking(scoreData *scoreList);

#endif
