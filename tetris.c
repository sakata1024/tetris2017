﻿#include "Tetris.h"
#include "Game.h"
#include "Ranking.h"
#include "Title.h"

int LINE;    //画面に映るステージの高さ
int COL;     //画面に映るステージの幅
int L_COL;   //ステージの左壁の座標
int LOOPWAIT;//ループするときのカウント最大値
int HEIGHT;  //ステージ全体の高さ
int WIDTH;   //ステージ全体の幅
int R_COL;   //ステージの右壁の座標

void Window_Init();
void Size_Init(int Scr_X, int Scr_Y);

//メイン関数
int main() {
  int finflag = 0;
  
  initscr(); //curses初期化
  
  Window_Init(); //画面情報の初期化
  
  //終了されるまでループ
  while (!finflag) {
    erase();
    
    switch (Title()) {
    /* ゲーム開始 */
    case 0:
      MainLoop();
      break;
      
    /* ランキング表示 */
    case 1:
      Ranking();
      break;
      
    /* ゲーム終了 */
    case 2:
      finflag = 1;
      break;
    }
  }
  
  endwin(); //cursesの終了

  return 0;
}

//画面情報の初期化をする関数
void Window_Init() {
  
  //画面初期化
  cbreak();
  start_color();
  noecho();
  curs_set(0);
  keypad(stdscr, TRUE);
  timeout(0);
  
  //色の設定
  init_pair(1, COLOR_CYAN, COLOR_BLACK);
  init_pair(2, COLOR_YELLOW, COLOR_BLACK);
  init_pair(3, COLOR_MAGENTA, COLOR_BLACK);
  init_pair(4, COLOR_BLACK, COLOR_BLACK);
  init_pair(5, COLOR_WHITE, COLOR_BLACK);
  init_pair(6, COLOR_GREEN, COLOR_BLACK);
  init_pair(7, COLOR_RED, COLOR_BLACK);
  
  init_pair(8, COLOR_WHITE, COLOR_BLACK);
  
  bkgd(COLOR_PAIR(8));
  
  //乱数シード値の生成
  srand((unsigned int)time(NULL));
  
  //画面サイズの設定
  Size_Init(COLS, LINES);
  
}

//サイズを初期化する関数
void Size_Init(int Scr_X, int Scr_Y) {
  LINE = Scr_Y - 1;
  COL = Scr_X / 4 - 2;
  L_COL = 20;
  LOOPWAIT = 25000;
  HEIGHT = Scr_Y + 1;
  WIDTH = COL;
  R_COL = L_COL + WIDTH + 1;
}
